# Thanks to https://fenicsproject.org/qa/2878

import pathlib
import dolfin
import sys
import mshr

def generate_all() :

    cases = {
        'B'    : [3],
        'BDFM' : [2],
        'BDM'  : [1],
        'CG'   : [1,2],
        'CR'   : [1],
        'DG'   : [0,1,2],
        'R'    : [0],
        'RT'   : [1]}

    for family in cases :
        for degree in cases[family] :
            print(f"Working on {family} {degree}")
            main(family,degree)
            print("-o"*20)

def init() :

    global mesh, mesh2

    pathlib.Path('results').mkdir(parents=True, exist_ok=True)
    
    domain = mshr.Circle(dolfin.Point(0.,0.),1.0,10)
    mesh = mshr.generate_mesh(domain, 3)
#    mesh = dolfin.UnitSquareMesh(5,5)    

    domain = mshr.Circle(dolfin.Point(0.,0.),1.0,10)
    mesh2 = mshr.generate_mesh(domain, 80)
#    mesh2 = dolfin.UnitSquareMesh(50,50)    
            
def main(family, degree) :

    pvd_mesh = dolfin.File('results/mesh.pvd')
    pvd_mesh << mesh

    V = dolfin.FunctionSpace(mesh, family, degree)
    v = dolfin.Function(V)

    # How can I know here the rank of V?

    V2 = dolfin.FunctionSpace(mesh2, family, degree)
    v2 = dolfin.interpolate(v, V2)
    
    pvd_file = dolfin.File(f'results/basis_{family}_{degree}_.pvd')

    N = len(v.vector())
    print(f"This basis has {N} functions.")

    for i in range(N) :
        if N > 10 and i % (N//10) == 0 :
            print(f"Working on function: {i}")
        if i != 0 :
            v.vector()[i-1] = 0
        v.vector()[i] = 1
        v2 = dolfin.interpolate(v, V2)
        v2.rename ("phi", "phi")
        pvd_file << v2

if __name__ == '__main__':

    family = sys.argv[1]      if len(sys.argv) >= 2 else None
    degree = int(sys.argv[2]) if len(sys.argv) >= 3 else None

    init()
    if family is None :
        generate_all()
    else :
        main(family, degree)
